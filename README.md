#A03 Emi calculator with Guestbook Example

A website project   using Nodemon, Express, BootStrap, EJS

## How to use

Open a command window in your c:\44563\A03Jarupati folder.

Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> nodemon 
```

Point your browser to `http://localhost:8081`. 